<?php

namespace Drupal\manage_comment_own_content;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\comment\Entity\CommentType;

/**
 * Provides dynamic override permissions for different comment types.
 */
class CommentPermissions {

  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * Returns an array of additional permissions.
   *
   * @return array
   *   An array of permissions.
   */
  public function commentTypePermissions() {
    $permissions = [];
    $specific_permission = [];
    $this->addGeneralPermissions($permissions);
    $specific_permission = $this->generatePermissions(CommentType::loadMultiple(), [$this, 'addSpecificPermissions']);
    return array_merge($permissions, $specific_permission);
  }

  /**
   * Add general permissions.
   *
   * @param array $permissions
   *   The permissions array, passed by reference.
   */
  private function addGeneralPermissions(array &$permissions) {
    $permissions['view overview of comments on own content'] = [
      'title' => $this->t('Access the comments view on user/x/comments'),
    ];
  }

  /**
   * Returns a list of comment type specific permissions.
   *
   * @param \Drupal\node\Entity\NodeType $type
   *   The node type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  private function addSpecificPermissions(CommentType $comment_type) {
    $type = $comment_type->id();
    $label = $comment_type->label();

    return [
      "$type view unpublished comments on own content" => [
        'title' => $this->t("%name: View unpublished comments on own content.", ["%name" => $label]),
      ],
      "$type delete comments on own content" => [
        'title' => $this->t("%name: Delete comments on own content.", ["%name" => $label]),
      ],
      "$type update comments on own content" => [
        'title' => $this->t("%name: Update comments on own content.", ["%name" => $label]),
      ],
    ];
  }

}
